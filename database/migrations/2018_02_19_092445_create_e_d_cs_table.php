<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEDCsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edcs', function (Blueprint $table) {
            $table->increments('ticket_id');
            $table->string('user_id');
            $table->string('error_status');
            $table->string('error_description');
            $table->string('report_by');
            $table->string('fixed_by');
            $table->timestamps();
            $table->dataTime('accepted_at');
            $table->string('fixed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edcs');
    }
}
