<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merchant;
use App\Employee;
use App\Technician;
use Auth;

class AuthController extends Controller
{
    public function registerMerchant(Request $request, Merchant $merchant)
    {
        $this->validate($request, [
            'user_id'       => 'required|unique:merchants',
            'password'      => 'required|min:5|max:15',
            'merchant_name' => 'required',
        ]);

        $merchant->create([
            'user_id'       => $request->user_id,
            'password'      => bcrypt($request->password),
            'merchant_name' => $request->merchant_name,
            'api_token'     => bcrypt($request->user_id)
        ]);
    }

    public function registerEmployee(Request $request, Employee $employee)
    {
        $this->validate($request, [
            'employee_id'   => 'required|unique:employees',
            'password'      => 'required|min:5|max:15',
            'employee_name' => 'required',
        ]);

        $employee->create([
            'employee_id'   => $request->employee_id,
            'password'      => bcrypt($request->password),
            'employee_name' => $request->employee_name,
            'api_token'     => bcrypt($request->employee_id)
        ]);
    }

    public function registerTechnician(Request $request, Technician $technician)
    {
        $this->validate($request, [
            'technician_id'     => 'required|unique:technicians',
            'password'          => 'required|min:5|max:15',
            'technician_name'   => 'required',
        ]);

        $technician->create([
            'technician_id'     => $request->technician_id,
            'password'          => bcrypt($request->password),
            'technician_name'   => $request->technician_name,
            'api_token'         => bcrypt($request->technician_id)
        ]);
    }

    public function loginMerchant(Request $request, Merchant $merchant) 
    {
        $error1 = array( 'errorCode' => 1,
            'errorTitle' => 'Wrong Credential',
            'errorMessage' => 'Invalid User ID or Password!');

        $noError = array( 'errorCode' => 0,
            'errorTitle' => null,
            'errorMessage' => null);

        if (!Auth::attempt(['user_id' => $request->user_id, 'password' => $request->password])){
            return response()->json([
                'status' => 0,
                'error' => $error1,
                ]);
        }

        $merchant = $merchant->find(Auth::user()->id);
        return response()->json([
            'status' => 1,
            'merchantId'=> $merchant->user_id, 
            'merchantName' => $merchant->merchant_name,
            'accessToken' => $merchant->api_token,
            'error' => $noError,
            ]);
    }

    public function loginEmployee(Request $request) 
    {
        $error1 = array( 'errorCode' => 1,
            'errorTitle' => 'Wrong Credential',
            'errorMessage' => 'Invalid Employee ID or Password!');

        $noError = array( 'errorCode' => 0,
            'errorTitle' => null,
            'errorMessage' => null);

        if (!Auth::guard('employee')->attempt(['employee_id' => $request->employee_id, 'password' => $request->password])){
            return response()->json([
                'status' => 0,
                'error' => $error1,
                ]);
        }

        try {
            $employee = Employee::find($request->employee_id);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ]);
        }

        return response()->json([
            'status' => 1,
            'employeeId'=> $employee->employee_id, 
            'employeeName' => $employee->employee_name,
            'accessToken' => $employee->api_token,
            'error' => $noError,
            ]);
    }

    public function loginTechnician(Request $request) 
    {
        $error1 = array( 'errorCode' => 1,
            'errorTitle' => 'Wrong Credential',
            'errorMessage' => 'Invalid Technician ID or Password!');

        $noError = array( 'errorCode' => 0,
            'errorTitle' => null,
            'errorMessage' => null);

        if (!Auth::guard('technician')->attempt(['technician_id' => $request->technician_id, 'password' => $request->password])){
            return response()->json([
                'status' => 0,
                'error' => $error1,
                ]);
        }

        try {
            $technician = Technician::find($request->technician_id);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ]);
        }

        return response()->json([
            'status' => 1,
            'technicianId'=> $technician->technician_id, 
            'technicianName' => $technician->technician_name,
            'accessToken' => $technician->api_token,
            'error' => $noError,
            ]);
    }
}
