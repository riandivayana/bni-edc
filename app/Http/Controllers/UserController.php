<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Merchant;

class UserController extends Controller
{
    public function merchants(Merchant $merchant)
    {
        $merchant = $merchant->all();
        return response()->json($merchant);
    }

}
