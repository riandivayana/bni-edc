<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Employee extends Authenticatable
{

    public $incrementing = false;
    protected $guard = 'employee';
    protected $primaryKey = "employee_id";
    use Notifiable;

    protected $fillable = [
        'employee_id', 'password', 'employee_name', 'api_token',
    ];

    protected $hidden = [
        'password',
    ];
}