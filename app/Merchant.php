<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Merchant extends Authenticatable
{

    use Notifiable;

    protected $fillable = [
        'user_id', 'password', 'merchant_name', 'api_token',
    ];

    protected $hidden = [
        'password',
    ];
}
