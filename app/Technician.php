<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Technician extends Authenticatable
{
    public $incrementing = false;
    protected $guard = 'technician';
    protected $primaryKey = "technician_id";

    use Notifiable;

    protected $fillable = [
        'technician_id', 'password', 'technician_name', 'api_token',
    ];

    protected $hidden = [
        'password',
    ];
}
