<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('registerMerchant', 'AuthController@registerMerchant');
Route::post('registerEmployee', 'AuthController@registerEmployee');
Route::post('registerTechnician', 'AuthController@registerTechnician');
Route::post('loginMerchant','AuthController@loginMerchant');
Route::post('loginEmployee','AuthController@loginEmployee');
Route::post('loginTechnician','AuthController@loginTechnician');
Route::get('merchants', 'UserController@merchants');
